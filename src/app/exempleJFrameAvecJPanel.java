import java.awt.Color;

import javax.swing.JFrame;
import javax.swing.JPanel;


public class exempleJFrameAvecJPanel {

	 
	public static void main(String[] args) {
	 
	 JFrame f = new JFrame();
	
	 f.setSize(400, 400);//dimensions 
	 
	 f.setTitle("Ma premi�re fen�tre swing");//titre
	 
     f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);//fermeture de la fenetre -> arret du programme
     
     //Position de la fenetre 
     f.setLocation(100,100);
     //f.setLocationRelativeTo(null);
     
     //bouton et contours
     //f.setUndecorated(true);
     
     //cr�ation d'un objet JPanel
     JPanel p = new JPanel();
     //D�finition de sa couleur de fond
     p.setBackground(Color.BLUE);        
     //On affecte le JPanel � notre JFrame 
     f.setContentPane(p);
     
     //visibilit�
     f.setVisible(true);
     
    
	}

}
