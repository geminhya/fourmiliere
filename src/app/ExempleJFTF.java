import java.awt.Color;
import java.awt.Dimension;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.NumberFormat;

import javax.swing.JFormattedTextField;
import javax.swing.JFrame;

public class ExempleJFTF extends JFrame implements ActionListener {
	
	JFormattedTextField jtf;

	ExempleJFTF() {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setLocationRelativeTo(null);
		jtf = new JFormattedTextField(NumberFormat.getIntegerInstance());
		jtf.setPreferredSize(new Dimension(150, 30));
		jtf.setForeground(Color.BLUE);
		jtf.addActionListener(this);
		getContentPane().add(jtf);
		pack();
		setVisible(true);
	}

	@Override
	public void actionPerformed(ActionEvent arg0) {
		System.out.println("Vous avez tap� " + jtf.getText());

	}
	public static void main(String[] args) {
		new ExempleJFTF();

	}

}
