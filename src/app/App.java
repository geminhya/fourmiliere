package app;

import javax.swing.JButton;
import javax.swing.JFrame;

public class App {
	public static void main(String[] args){
		System.out.println("Java Avancé - TP 6 - Swing\n");
		JFrame f = new JFrame();
		f.setSize(400, 400);
		f.setTitle("Ma première fenêtre swing");
		f.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		JButton jb = new JButton();
		f.getContentPane().add(jb);
		f.setVisible(true);
	}
}
